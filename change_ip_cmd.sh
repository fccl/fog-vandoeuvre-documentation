#! /bin/bash

#Recherche nouvelle IP
ip=`ip route get 1.2.3.4 | awk '{print $7}'`
echo "IP trouvée : $ip"

#Recherche ancienne IP
fichier=/opt/fog/.fogsettings
ip_old=`grep  -e "ipaddress" "$fichier" | cut -d\' -f2`
echo "Ancienne IP utilisée : $ip_old"

#Fonction pour remplacer les IP dans un fichier.
Remplacement_ip()
{
sed -i "s/$ip_old/$ip/" "$fichier"
}

#Verification des IP
Verif=N
read -p "Les IP sont elles correctes (Taper O si oui, N si non) ? " Verif

if [ $Verif = "O" ];then
        #Remplacement dans les fichiers
        fichier=/etc/dnsmasq.d/ltsp.conf
        Remplacement_ip
        fichier=/opt/fog/.fogsettings
        Remplacement_ip
        
        #Remplacement dans la base de donnée, permet d'éviter la partie graphique
        echo "update globalSettings set settingValue='$ip' where settingValue='$ip_old';" | mysql -b fog
        echo "update  nfsGroupMembers set ngmHostname='$ip'  where ngmHostname = '$ip_old';" | mysql -b fog

        #Installation certificat pour l'IP
        printf "%b" "Y\n\n" | /root/fogproject/bin/installfog.sh -C -K

        #Redémarage du serveur
        reboot

else
        echo "Fin du script ... erreur"
fi
