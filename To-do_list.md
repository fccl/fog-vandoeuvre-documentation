# Prioritaires

- Compléter le script (change_ip_cmd.sh) qui change l’IP local, en prenant en charge les modifs dans la BDD et le redémarrage du serveur. -> Téléversé sur dans le point 10.

- Augmenter le temps de sélection lors du démarrage en PXE pour faciliter l’enregistrement des machines (par défaut 5 secondes).
Fog graphique -> Configuration FOG -> IPXE Genral COnfiguration -> Menu colors, pairing, settings -> Menu timeout (in seconds) = n 
avec n le nombre de secondes voulues

- Modifier la forme de la documentation en y intégrant des liens entre les page
Fait manque commit

- Ajouter référence au paquet ssh-askpass à ajouter sur le client virt-manager via Debian / Ubuntu.
- Améliorer la partie concernant l'accès aux serveurs par clé SSH. Ajouter en « annexe » la démarche : Créer une clef si pas déjà fait, l'envoyer sur le serveur hôte et la vm.

# Secondaires

- Développer l’usage du Multicast lors de déploiement en masse de clones simultanément (sur un parc informatique sûr).
- Redimensionner les partitions à l’aide de commandes via Snapin (Linux uniquement).
