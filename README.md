<div align="center">
<img src="Documentation/Images/logo.png" alt="Logo FVD">
<p>
    <em>Le clonage de PC facile pour toutes les mairies !</em>
</p>
</div>

[Fog](http://fogproject.org/) est un logiciel qui permet de déployer des images disques en utilisant PXE et qui permet d'administrer à distance le système des ordinateurs d'un réseau.

[Vandœuvre-lès-Nancy](https://www.vandoeuvre.fr/) est la deuxième commune la plus peuplée de la Métropole du Grand Nancy et du département français de Meurthe-et-Moselle dans le Grand Est.

Le but de la documentation est de vous apprendre à installer, configurer et utiliser Fog dans le cadre de clonage de PC dans les mairies.

# Sommaire

0. [Avant de commencer](Documentation/0_Avant_de_commencer.md)
1. [Installation de Debian sur la machine hôte](Documentation/1_Installation_Debian_hote.md)
2. [Configuration du WakeOnLan](Documentation/2_Configuration_WOL.md)
3. [Installation d'un système de virtualisation](Documentation/3_Installation_systeme_virtualisation.md)
4. [Installation de Debian sur la machine virtuelle](Documentation/4_Installation_Debian_VM.md)
5. [Configuration de Fog Project](Documentation/5_Configuration_Fog.md)
6. [Capture d'un système d'exploitation](Documentation/6_Capture_systeme_exploitation.md)
7. [Déploiement d'une image dans un parc informatique](Documentation/7_Deploiement_image.md)
8. [Installation du client Fog](Documentation/8_Installation_client_Fog.md)
9. [Déploiement de logiciels dans un parc informatique](Documentation/9_Deploiement_logiciel.md)
10. [Changement d'adresse IP du serveur](Documentation/10_Changement_adresse_IP.md)

# Contribution

Si vous souhaitez contribuer, n'hésitez pas à ouvrir une **issue**, toute contribution est appréciée et elle aide au développement de la documentation.

# Licence

La documentation est sous la licence **CC BY-SA 4.0**, excepté les logos qui conservent leur licence d'origine, tout comme les technologies utilisées.