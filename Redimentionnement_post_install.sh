#! /bin/bash

#Environnement de travail : ordinateur avec maximum 9 partitions dont au moins une de données (/home).
#Après déploiement via FOG, on souhaite que la partition /home (la dernière s'il y en a plusieurs) prenne toute la place disponible sur le disque.
#Ce script est là pour ça.


#Vérification de la présence de parted sur le système. Si non, il est installé.
Verif=$(which parted)
if  [ -z $Verif ]
then
   sudo apt install parted -y
else
   echo "Parted installé, redimentionnement possible"
fi

#Détermination numéro (sdaN) de la partion /home
n=$(lsblk -o MOUNTPOINT,NAME | grep -e "^/home.*sda")
n=${n:(-1)}
echo "Partion à agrandir : sda$n"

#Modification la taille de la partition n°3 (celle des données). 
sudo parted /dev/sda ---pretend-input-tty <<EOF 
 print
 resizepart $n
 yes
 -0
 print
 q
EOF
#Dans l'ordre des commandes : 
#print -> Permet de visualiser le dimentionnement des partitions de sda avec bornes de début et de fin.
#resizepart n -> On modifie la parttion n°n qui correspond à la partition /home (des données)
#yes -> Oui on accepte de modifier une partition montée
#-0 -> Si on veut prendre toute la place disponible. 230GB -> La limite de fin de la partition est à fixée à cette 		valeur. Si on réduit la partition, il faut rajouter un "yes" sur la ligne suivante.
#print -> Verification de la modification
#q -> quitter parted
