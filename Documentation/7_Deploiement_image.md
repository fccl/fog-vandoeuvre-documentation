# Déploiement d'une image dans un parc informatique

---

Dans cette septième partie, on va **déployer** une image dans un parc informatique :

1. Nettoyez les PCs dont vous souhaitez faire le déploiement avec GParted (formatez les disques en **MBR** si vous déployez sur des PCs avec des disques plus petits ou **GPT** par défaut).

![](Images/gparted.jpg)

2. Bootez les PCs en mode **PXE** et choisissez **Quick Registration and Inventory**.

![](Images/menu.png)

3. Une fois les PCs enregistrés, allez dans **hôtes > List All Hosts** et vous y trouverez les PCs.

![](Images/hotes.png)

4. Personnalisez-les en cliquant sur leur **Host**, modifiez leur **Nom d'hôte**, leur **Host description** et sélectionnez l'image désirée dans **image hôte** et cliquez sur **Mettre à jour**.

![](Images/modif.png)

5. Enfin, cliquez sur **Basic Tasks > Deploy** et cliquez sur **Tâche**.

![](Images/deploy.png)

6. Vous pouvez voir dans **les tâches** que Fog surveille les machines afin de pouvoir y déployer le système d'exploitation.

![](Images/task1.png)

7. Éteignez les PCs, bootez-les en mode **PXE** et le déploiement commence.

![](Images/partclone1.png)

![](Images/load1.png)

8. Ça y est, l'OS est déployé, vous pouvez le voir sur les PCs et dans **hôtes > List All Hosts**.

![](Images/fedora1.png)

![](Images/deploye.png)

## La partie 7 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](6_Capture_systeme_exploitation.md) - [suivant](8_Installation_client_Fog.md) </div>
</div>