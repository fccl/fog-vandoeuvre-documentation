# Installation du client Fog

---

Dans cette huitième partie, on va installer le **client Fog** sur des machines **Linux** afin de pouvoir y **déployer** des logiciels/scripts à **distance** :

1. Installez sur votre distribution **mono-complete**, il vous faut la version 4.2 minimum.

```
sudo dnf install mono-complete
mono -V
```

![](Images/mono.png)

2. Retournez sur l'interface Web de Fog, cliquez sur **Fog Client** et téléchargez l'exécutable **Smart Installer (Recommended)**.

![](Images/exe.png)

3. Enfin, installez-le avec **mono** et répondez aux questions avec les réponses qui suivent :

```
sudo mono SmartInstaller.exe
```

```
Fog Server address [default: fogserver]: <IP_de_Fog>
Webroot [default: /fog]: [Enter]
Enable tray icon? [Y/n]: [Enter]
Start Fog Service when done? [Y/n]: [Enter]
```

![](Images/smartinstall.png)

## La partie 8 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](7_Deploiement_image.md) - [suivant](9_Deploiement_logiciel.md) </div>
</div>