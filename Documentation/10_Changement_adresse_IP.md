# Changement d'adresse IP du serveur

---

Dans cette dixième partie, on va **modifier** la configuration du serveur pour qu'il **marche** quand on lui **change** son adresse **IP** :

1. Modifiez le fichier **/etc/dnsmasq.d/ltsp.conf** en changeant l'ancien IP de Fog avec le nouveau :

```
dhcp-boot=undionly.kpxe,,<fog_server_ip>
dhcp-boot=net:UEFI32,i386-efi/ipxe.efi,,<fog_server_ip>
dhcp-boot=net:UEFI,ipxe.efi,,<fog_server_ip>
dhcp-boot=net:UEFI64,ipxe.efi,,<fog_server_ip>
dhcp-range=<fog_server_ip>,proxy
```

![](Images/ltsp.png)

2. Modifiez le fichier **/opt/fog/.fogsettings** en changeant l'ancien IP de Fog avec le nouveau :

```
ipaddress=<fog_server_ip>
```

![](Images/fogsettings.png)

3. Relancez l'installeur afin de créer un nouveau certificat pour l'IP :

```
./installfog.sh -C -K
```

![](Images/cert.png)

4. Sur l'interface Web, allez dans **Stockage > DefaultMember** et modifiez **Adresse IP**.

![](Images/node.png)

5. Enfin, allez dans **Configuration FOG > FOG Settings** et modifiez **TFTP HOST** de **TFTP Server** et **WEB HOST** de **Web Server**.

![](Images/tftp.png)

![](Images/web.png)

6. Redémarrez le serveur et ça fonctionne.

![](Images/saved.png)

## Bonne utilisation de Fog :)

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](9_Deploiement_logiciel.md) - [suivant](11_Aller_plus_loin.md)</div>
</div>
