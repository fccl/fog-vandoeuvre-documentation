# Installation du système de virtualisation

---

Dans cette troisième partie, on va installer **libvirt** sur la machine hôte afin de pouvoir émuler **Debian 10** sur celle-ci :

1. Installez les paquets suivants sur la machine.

```
apt install qemu-utils dnsmasq netcat-openbsd bridge-utils dnsmasq-base iptables network-manager
```

![](Images/dnsmasq.png)

2. Ouvrez le fichier **/etc/network/interfaces** et copiez-collez le code suivant en modifiant le nom de l'interface par le deuxième de tout à l'heure.

```
# The primary network interface
allow-hotplug <deuxieme_interface_reseau>
iface <deuxieme_interface_reseau> inet dhcp
# This is an autoconfigured IPv6 interface
iface <deuxieme_interface_reseau> inet6 auto
```

![](Images/iface.png)

3. Créez le fichier **/etc/network/interfaces.d/br0** et complétez-le avec le code :

```
## DHCP ip config file for br0 ##
auto br0

# Bridge setup
 iface br0 inet dhcp
    bridge_ports <deuxieme_interface_reseau>
```

![](Images/br0.png)

4. Redémarrez network-manager et installez les paquets suivants sur la machine.

```
systemctl restart NetworkManager
apt install --no-install-recommends qemu-system libvirt-clients libvirt-daemon-system
```

![](Images/noinstall.png)

5. Ajoutez l'utilisateur au groupe **libvirt** et activez le nat.

```
adduser fog libvirt
virsh --connect=qemu:///system net-autostart default
```

![](Images/virsh.png)

6. Installez wget et récupérez **Debian 10** dans le home de fog.

```
apt install wget
wget https://cdimage.debian.org/cdimage/archive/10.12.0/amd64/iso-cd/debian-10.12.0-amd64-netinst.iso
```

![](Images/wget.png)

7. Connectez-vous sur la machine avec Virt-manager.

![](Images/login.png)

8. Suivez la [vidéo](Videos/vm.mkv) afin de créer la machine virtuelle.

9. Ça y est, la machine est créée, n'oubliez pas de faire des snapshots et des sauvegardes pour ne pas perdre votre travail.

```
qemu-img convert -O qcow2 debian10.qcow2 sauvegarde/<nom_sauvegarde>.qcow2
```

![](Images/snapshot.png)

![](Images/convert.png)

## La partie 3 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](2_Configuration_WOL.md) - [suivant](4_Installation_Debian_VM.md) </div>
</div>