# Installation de Debian sur la machine virtuelle

---

Dans cette quatrième partie, on va installer **Debian 10** sur la machine virtuelle étant actuellement, la dernière version fonctionnant avec Fog :

1. Une fois la machine en route, choisissez **Graphical install** et sélectionnez le français pour la langue, la localisation et le clavier.

![](Images/langue2.png)

2. Donnez comme nom de machine **fog** et laissez le domaine vide.

![](Images/nom2.png)

3. Entrez un mot de passe fort pour le superutilisateur.

![](Images/superuser2.png)

4. Donnez comme nom au nouvel utilisateur **fog** et entrez un mot de passe fort.

![](Images/identifiant2.png)

5. Choisissez la méthode de partitionnement Manuel et suivez la suite de la procédure dans cette [vidéo](Videos/lvm.mkv).

6. Laissez l'analyse d'un autre support à **Non** et continuez.

![](Images/dvd.png)

7. Sélectionnez **France** et choisissez **deb.debian.org** pour le miroir et laissez le mandataire vide.

![](Images/miroir2.png)

8. Répondez **Non** au popularity-contest et ne cochez que **serveur SSH** lors de la sélection des logiciels.

![](Images/logiciels2.png)

9. Choisissez **Oui** à l'installation du GRUB et sélectionnez **/dev/sda** comme périphérique.

![](Images/grub2.png)

10. Ça y est, l'installation est terminée, cliquez sur **Continuer**.

![](Images/fin.png)

## La partie 4 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](3_Installation_systeme_virtualisation.md) - [suivant](5_Configuration_Fog.md) </div>
</div>