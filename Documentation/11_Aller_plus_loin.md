# Clef SSH

Lorsque l'accès au serveur Fog est régulier, il est déplaisant d'indiquer le mot de passe à chaque connexion. Ainsi générer une clef ssh, permet de se connecter aux machines, depuis un ordinateur identifié, sans indiquer le mot de passe.

## Générer une clef SSH

Si cela n'est pas déjà fait, dans le doute vous pouvez vérifier dans votre répertoire dédié .ssh s'il n'existe pas déja un fichier .pub (`~/.ssh).

    ssh-keygen

Il est possible d'y ajouter des options, pour cette documentation est proposé la solution la plus simple : valider les questions sans indiquer d'information.

## Envoyer la clef sur les serveurs

Lorsque la clef ssh, le serveur hote et la machine virtuelle sont opérationnels, envoyer la clef sur les serveurs :

    ssh-copy-id -i ~/.ssh/nom_de_la_clef_ssh.pub fog@ip_local

Il faudra valider l'envoie avec le mot de passe du serveur / machine virtuelle.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](10_Changement_adresse_IP.md) </div>
</div>