# Avant de commencer

---

Avant de commencer à se lancer dans l'installation de Fog, il vous faut :

- Un PC dont le Wake-on-LAN et l'émulation sont activés dans le BIOS

- 2 câbles Ethernet

- 2 disques durs

- Un chargeur

- Un accès non-filtré au réseau et en IP fixe

- Un accès aux serveurs par clé SSH (ssh-keygen et ssh-copy-id)

Et dans le PC qui gère Fog, il vous faut :

- Une distribution Linux (Fedora, Debian, OpenSuse, etc.)
- Virt-manager 4.0.0 minimum

Une fois tous les critères répondus, vous pouvez passer à l'étape 1, si vous n'avez pas tout, vous pouvez continuer, mais il faudra s'adapter en fonction de ce que vous avez.

<div align="center">
<img src="Images/machine_fog.jpg" alt="Machine Fog">
<p>
    <em>Le PC où j'ai installé Fog !</em>
</p>
</div>

<div>
<hr/>
<div align="left">
[retour](../README.md)</div><div align="right"> [suivant](1_Installation_Debian_hote.md) </div>
</div>