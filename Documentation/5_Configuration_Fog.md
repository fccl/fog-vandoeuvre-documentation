# Configuration de Fog Project

---

Dans cette cinquième partie, on va **installer** et **configurer** Fog Project :

1. Connectez-vous en root et installez **git** sur la machine.

```
su -
apt install git -y
```

![](Images/git.png)

2. Clonez Fog dans le dossier **root** et allez dans le dossier **bin**.

```
cd /root
git clone https://github.com/FOGProject/fogproject.git
cd fogproject/bin/
```

![](Images/clone.png)

3. Exécutez le script **installfog.sh** et remplissez les questions avec les données qui suivent :

```
Version Linux : 2 (Debian)
Mode installation : N (Normal)
Changer interface réseau ? N (enp[...])
Adresse routeur sur le serveur DHCP ? N
Le serveur DHCP gère le DNS ? N
Fog gère le serveur DHCP : N
Installer les traductions ? Y
Installer TLS pour passer l'interface en HTTPS ? Y
Changer le nom de la machine (serveur VM) ? N
```

![](Images/fog.png)

4. Allez sur le lien donné et cliquez sur **Install/Update Now** pour mettre la base de données à jour.

```
/!\ Le certificat étant auto-signé, le navigateur affichera qu'il y a un risque probable de sécurité, car nous ne sommes pas connus, mais comme le chiffrement fonctionne, il faut accepter le risque et poursuivre. /!\
```

![](Images/bddcmd.png)

![](Images/bddweb.png)

5. Une fois cela fait, retournez sur le terminal et faite [Enter] pour continuer la configuration.

![](Images/fincmd.png)

![](Images/finweb.png)

6. La configuration étant terminée, installez **dnsmasq**.

```
apt install dnsmasq -y
```

![](Images/dnsmasq2.png)

7. Créez le fichier **/etc/dnsmasq.d/ltsp.conf** et complétez le document avec les données suivantes en mettant bien l'IP de Fog dans les endroits convenus :

```
# Don't function as a DNS server:
port=0

# Log lots of extra information about DHCP transactions.
log-dhcp

# Set the root directory for files available via FTP.
tftp-root=/tftpboot

# The boot filename, Server name, Server Ip Address
dhcp-boot=undionly.kpxe,,<fog_server_IP>

# Disable re-use of the DHCP servername and filename fields as extra
# option space. That's to avoid confusing some old or broken DHCP clients.
dhcp-no-override

# inspect the vendor class string and match the text to set the tag
dhcp-vendorclass=BIOS,PXEClient:Arch:00000
dhcp-vendorclass=UEFI32,PXEClient:Arch:00006
dhcp-vendorclass=UEFI,PXEClient:Arch:00007
dhcp-vendorclass=UEFI64,PXEClient:Arch:00009

# Set the boot file name based on the matching tag from the vendor class (above)
dhcp-boot=net:UEFI32,i386-efi/ipxe.efi,,<fog_server_IP>
dhcp-boot=net:UEFI,ipxe.efi,,<fog_server_IP>
dhcp-boot=net:UEFI64,ipxe.efi,,<fog_server_IP>

# PXE menu.  The first part is the text displayed to the user.  The second is the timeout, in seconds.
pxe-prompt="Booting Fog Client", 1

# The known types are x86PC, PC98, IA64_EFI, Alpha, Arc_x86,
# Intel_Lean_Client, IA32_EFI, BC_EFI, Xscale_EFI and X86-64_EFI
# This option is first and will be the default if there is no input from the user.
pxe-service=X86PC, "Boot to Fog", undionly.kpxe
pxe-service=X86-64_EFI, "Boot to Fog UEFI", ipxe.efi
pxe-service=BC_EFI, "Boot to Fog UEFI PXE-BC", ipxe.efi

dhcp-range=<fog_server_ip>,proxy
```

![](Images/ltsp.png)

8. Enfin, redémarrez dnsmasq et activez-le au démarrage pour prendre en compte le changement.

```
systemctl restart dnsmasq.service
systemctl enable dnsmasq.service
```

![](Images/systemctl.png)

9. Retournez sur le lien donné et connectez-vous avec comme identifiant **fog** et mot de passe **password**.

![](Images/fog_login.png)

10. Allez dans **Users > List All Users > fog > Change password** pour mettre un mot de passe plus sécurisé.

![](Images/passwd.png)

11. Pour finir, allez dans **Fog Configuration > Fog Settings > General Settings** pour mettre la langue en français.

![](Images/fr.png)

## La partie 5 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](4_Installation_Debian_VM.md) - [suivant](6_Capture_systeme_exploitation.md) </div>
</div>
