# Configuration du Wake-on-LAN

---

Dans cette deuxième partie, on va configurer le **Wake-on-LAN** :

1. Ajoutez la ligne suivante dans le fichier **/etc/network/interfaces**.

```
ethernet-wol g
```

![](Images/wolg.png)

2. Installer **ethtool** et faite la commande suivante pour activer le **Wake-on**.

```
apt install ethtool
ethtool -s <premiere_interface_reseau> wol g
ethtool <premiere_interface_reseau>
```

![](Images/ethtool.png)

3. Ça y est, le WoL est activé, plus qu'à éteindre la machine et à la réveiller.

```
wol <adresse_mac>
```

![](Images/wol.png)

## La partie 2 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](1_Installation_Debian_hote.md) - [suivant](3_Installation_systeme_virtualisation.md) </div>
</div>