# Installation de Debian sur la machine hôte

---

Dans cette première partie, on va installer **Debian 11** sur la machine hôte :

1. Gravez l'ISO sur une clé USB (avec Etcher par exemple) et bootez sur celle-ci.

![](Images/etcher.png)

2. Choisissez **Graphical install** et sélectionnez le français pour la langue, la localisation et le clavier.

![](Images/langue.png)

3. Sélectionnez la première interface réseau, la deuxième sera donnée à la machine virtuelle lors de sa création.

![](Images/reseau.png)

4. Donnez comme nom de machine **fog** et laissez le domaine vide.

![](Images/nom.png)

5. Entrez un mot de passe fort pour le superutilisateur.

![](Images/superuser.png)

6. Donnez comme nom au nouvel utilisateur **fog** et entrez un mot de passe fort.

![](Images/identifiant.png)

7. Choisissez la méthode de partitionnement Manuel et suivez la suite de la procédure dans cette [vidéo](Videos/raid-lvm.mkv).

8. Sélectionnez **France** et choisissez **deb.debian.org** pour le miroir et laissez le mandataire vide.

![](Images/miroir.png)

9. Répondez **Non** au popularity-contest et ne cochez que **serveur SSH** lors de la sélection des logiciels.

![](Images/logiciels.png)

10. Choisissez **Oui** à l'installation du GRUB et sélectionnez **/dev/sda** comme périphérique, on fera le sdb après.

![](Images/grub.png)

11. Ça y est, l'installation est terminée, retirez la clé et cliquez sur **Continuer**.

12. Une fois la machine redémarrée, connectez-vous en root et faites la commande suivante pour terminer l'installation du RAID.

```
su -
grub-install /dev/sdb
```

![](Images/sdb.png)

## La partie 1 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](0_Avant_de_commencer.md) - [suivant](2_Configuration_WOL.md) </div>
</div>