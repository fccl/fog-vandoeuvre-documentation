# Déploiement de logiciels dans un parc informatique

---

Dans cette neuvième partie, on va **exécuter** un script **bash** pour **installer** le logiciel VLC dans les PCs grâce au **Snapin** :

1. Créez un script Bash.

```
#!/bin/sh

echo faites les commandes que vous voulez ; sudo apt install vlc -y
```

![](Images/vlc.png)

2. Dans l'interface Web de Fog, allez sur **snapin > Create New Snapin**, complétez et cliquez sur **Ajouter** : 
- Le **Nom snapin**
- Le **snapin description**
- Choisissez le **snapin Template**
- Sélectionnez le script pour **snapin fichier**
- Désélectionnez **Redémarrez après l'installation**

![](Images/1.png)

![](Images/2.png)

3. Une fois cela fait, allez dans **hôtes > List All Hosts**, choisissez les PCs que vous voulez et cliquez sur **Basic Tasks > Avancée > Single Snapin**, prenez le snapin que vous voulez et cliquez sur **Tâche**.

![](Images/3.png)

4. Vous pouvez voir dans **les tâches** que Fog surveille les machines afin de pouvoir y déployer le script.

![](Images/4.png)

5. Attendez quelques instants et ça y est, le script est exécuté.

![](Images/fini.png)

## La partie 9 est terminée.

<div>
<hr/>
<div align="left">
[Retour](../README.md)</div><div align="right"> [Précédent](8_Installation_client_Fog.md) - [suivant](10_Changement_adresse_IP.md) </div>
</div>